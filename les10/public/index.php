<?php
    include __DIR__ . '/vendor/autoload.php';
    
    // instantie creëren van de getallen klasse
    $getallen = new \LilDew\LerenWerkenMetPHP\Getallen;
    $getallen->Optellen();
    $getallen->Aftrekken();
    $getallen->Vermenigvuldiging();
    $getallen->Deling();
    $getallen->modulusRestwaarde();
    $getallen->Macht();
    
    
    $boolean = new \LilDew\LerenWerkenMetPHP\Boolean;
    $boolean-> False();
    $boolean-> True();
    
    $tekst = new \LilDew\LerenWerkenMetPHP\Tekst;
    $tekst-> stringDeclareren();
    $tekst-> stringAanElkaarPlakken();
    $tekst-> stringInterpolatie();
    $tekst-> tekstValideren();
    $tekst-> stringsVergelijken();
    $tekst-> strCaseCmp();
    $tekst-> tekstOpmaken();
    $tekst-> zeroPadding();
    $tekst-> signModifier();
    $tekst-> hoofdLetters();
    $tekst-> kleineLetters();
    $tekst-> upperCaseWords();
    $tekst-> upperCaseFirst();
    $tekst-> lowerCaseFirst();
    $tekst-> subString();
    $tekst-> stringReplace();
  
?>