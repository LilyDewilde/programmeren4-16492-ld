<?php
namespace LilDew\LerenWerkenMetPHP;

class Tekst
{
    public function stringDeclareren()
    {
        $tekst = '"Ik ben aan het werken," zie ze.';
        print $tekst . '<br>';
    }
    
    public function stringAanElkaarPlakken()
    {
        $groet = 'Een goede morgen';
        $aan = 'Jan';
        $wens = 'toegewenst.';
        $zin = $groet . ' ' . $$aan . '  ' . $wens;
        echo $zin . '<br>';
    }
    
    public function stringInterpolatie()
    {
        $groet = 'Een goede morgen';
        $aan = 'Jan';
        $wens = 'toegewenst.';
        $zin = "$groet $aan $wens";
        echo $zin . '<br>';
    }
    
    public function tekstValideren()
    {
        $zipcode = trim($_POST['zipcode']);

        $zip_length = strlen($zipcode);
        if (strlen(trim($_POST['zipcode'])) != 4) {
        print 'Een postcode bestaat uit maximum 4 karakter.' . '<br>';}
    }
    
    public function stringsVergelijken()
    {
        $email = trim($_POST['email']);

        if($_POST['email'] == 'bob.dylan@telenet.be') {
        print 'Welkom, Bob Dylan.' . '<br>';}
    }
    
    public function strCaseCmp()
    {
        $name = trim($_POST['name']);
         
        if (strcasecmp($_POST['name'], 'Bob Dylan') == 0) {
        print "Welkom terug, Bob Dylan!" . '<br>';}
    }
    
    public function tekstOpmaken()
    {
        echo sprintf(Date("Y-m-d H:i"));
        
        $price = 5;
        $tax = 12;
        echo sprintf('De maaltijd kost $%.2f', $price  +  ($price/100 * $tax)) . '<br>';
    }
    public function zeroPadding()
    {
        $zip = '6520';
	    $month = 2;
    	$day = 6;
	    $year = 2018;
	    
	    echo sprintf("Postcode is %05d en de datum is %02d/%02d/%d", $zip, $month, $day, $year);
    }
    
    public function signModifier()
    {
        $min = -4;
    	$max = 32;

    	echo sprintf("Dit jaar was de minimum temperatuur %+d en de maximum %+d graden Celsius.", $min, $max);
    }
    
    public function hoofdLetters()
    {   
    
        echo sprintf(strtoupper ('victoires, café, brasserie, bAR'));
    }
    
    public function kleineLetters()
    {
        echo sprintf (strtolower ('Victoires, Café, Brasserie, BAR'));

    }
    
    public function upperCaseWords()
    {
        echo sprintf (ucwords('marios machikas'));

    }
    
    public function upperCaseFirst()
    {
        echo sprintf (ucfirst('marios machikas'));
    }
    
    public function lowerCaseFirst()
    {
        echo sprintf (lcfirst('Marios Machikas'));
    }
    
    public function subString()
    {
        echo 'Card: 5980-5448-8547-6456';
        
        echo substr($_POST['card'],-4,4);
    }
    
    public function stringReplace()
    {
        $cssClass = 'lunch';
        
        $html = '<span class = "{class}">Friet met mayonaise<span><span class = "{class}">Frikandel met ui</ span> ';
        
        echo sprintf (str_replace ('{class}', $cssClass, $html));
    }
}