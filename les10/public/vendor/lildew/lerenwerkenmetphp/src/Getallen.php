<?php
namespace LilDew\LerenWerkenMetPHP;

class Getallen
{
    public function Optellen()
    {
        $integer = 5;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        echo $integer + $integer2 . '<br>';
        echo $decimal + $decimal2 . '<br>';
        echo $float + $float2 . '<br>';
        echo $null + $integer . '<br>';
        echo $null + $decimal . '<br>';
        echo $null + $float . '<br>';
        echo $null + $negativeFloat . '<br>';
        echo $negative + $negativeInteger . '<br>';
        echo $float + $negativeFloat . '<br';
        echo $decimal3 + $decimal . '<br>';
    }
        
    public function Aftrekken()
    {
        $integer = 5;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        echo $integer - $integer2 . '<br>';
        echo $decimal - $decimal2 . '<br>';
        echo $float - $float2 . '<br>';
        echo $null - $integer . '<br>';
        echo $null - $decimal . '<br>';
        echo $null - $float . '<br>';
        echo $null - $negativeFloat . '<br>';
        echo $negative - $negativeInteger . '<br>';
        echo $float - $negativeFloat . '<br';
        echo $decimal3 - $decimal . '<br>';
    }
    
    public function Vermenigvuldiging()
    {
        $integer = 5;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        
        echo $integer * $integer2 . '<br>';
        echo $decimal * $decimal2 . '<br>';
        echo $float * $float2 . '<br>';
        echo $null * $integer . '<br>';
        echo $null * $decimal . '<br>';
        echo $null * $float . '<br>';
        echo $null * $negativeFloat . '<br>';
        echo $negative * $negativeInteger . '<br>';
        echo $float * $negativeFloat . '<br';
        echo $decimal3 * $decimal . '<br>';
    }
    
    public function Deling()
    {
        $integer = 5;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        
        echo $integer / $integer2 . '<br>';
        echo $decimal / $decimal2 . '<br>';
        echo $float / $float2 . '<br>';
        echo $null / $integer . '<br>';
        echo $null / $decimal . '<br>';
        echo $null / $float . '<br>';
        echo $null / $negativeFloat . '<br>';
        echo $negative / $negativeInteger . '<br>';
        echo $float / $negativeFloat . '<br';
        echo $decimal3 / $decimal . '<br>';
        
    }
        
    public function modulusRestwaarde()
    {
        $integer = 5;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        
        echo $integer % $integer2 . '<br>';
        echo $decimal % $decimal2 . '<br>';
        echo $float % $float2 . '<br>';
        echo $null % $integer . '<br>';
        echo $null % $decimal . '<br>';
        
        echo $null % $negativeFloat . '<br>';
        echo $negative % $negativeInteger . '<br>';
        echo $float % $negativeFloat . '<br>';
        echo $decimal3 % $decimal . '<br>';
    }
        
    public function Macht()
    {
        $integer = 5;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        
        echo pow($integer, $integer2 . '<br>');
        echo pow($decimal, $decimal2 . '<br>');
        echo pow($float, $float2 . '<br>');
        echo pow($null, $integer . '<br>');
        echo pow($null, $decimal . '<br>');
        echo pow($null, $float . '<br>');
        echo pow($null, $negativeFloat . '<br>');
        echo pow($negative, $negativeInteger . '<br>');
        echo pow($float, $negativeFloat . '<br>');
        echo pow($decimal, $decimal3 . '<br>');
    }
}
