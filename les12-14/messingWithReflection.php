<?php
include 'Team.php';

interface iTeamy {}

class Team2 extends Team implements iTeamy {}

$reflectTeam = new ReflectionClass(new Team2());
print(nl2br('methods:'));
foreach($reflectTeam->getMethods() as $methodName) {
    printf(nl2br($methodName));
}
print(nl2br('interfaces:'));
foreach($reflectTeam->getInterfaceNames() as $interfaceName) {
    printf(nl2br($interfaceName));
}
//print($reflectTeam->getParentClass());
//print($reflectTeam->getProperties());

//$reflectTeam->setNaam("reflectTeam");
//not an instance, can call newInstance to get one if you really wanted to
$teamInstanceFromReflection = $reflectTeam->newInstance();
$teamInstanceFromReflection->setNaam("reflectTeam");
//print(nl2br($teamInstanceFromReflection->getNaam()));

?>