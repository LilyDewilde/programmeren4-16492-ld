<?php
include('Speler.php');
// instantie creëren van de speler klasse
$speler = new \Competitie\Speler;
$speler->setFamilienaam('Dewilde');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP</title>
    <form>
        <fieldset>
            <legend>Speler</legend>
            <div>
            <label>Familienaam: </label>
            <input value="<?php echo $speler->getFamilienaam(); ?>">
            </div>
        </fieldset>
    </form>
</head>
<body>
<h1>Leren werken met PHP</h1>
</body>
</html>