<?php
// __DIR__ retourneert de naam van de map waarin index.php staat.
//composer autoload loads all model classes instead of individual includes
$currentMap = __DIR__;
echo $currentMap;
include __DIR__ . '/vendor/autoload.php';


// instantie creëren van de speler klasse
//use psr4 urls
$speler = new \LilDew\Competitie\Model\Speler;
$speler->setFamilienaam('Inghelbrecht');

$liga = new \LilDew\Competitie\Model\Liga;
$liga->setNaam('België');

$team = new \LilDew\Competitie\Model\Team;
$team->setNaam('De toppers');

$wedstrijd = new \LilDew\Competitie\Model\Wedstrijd;
$wedstrijd->setDatum(date('d/m/Y'));

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP</title>
    <form>
        <fieldset>
            <legend>Speler</legend>
            <div>
            <label>Familienaam: </label>
            <input value="<?php echo $speler->getFamilienaam(); ?>">
            </div>
        </fieldset>
        <fieldset>
            <legend>Liga</legend>
            <div>
            <label>Naam: </label>
            <input value="<?php echo $liga->getNaam(); ?>">
            </div>
        </fieldset>
        <fieldset>
            <legend>Team</legend>
            <div>
            <label>Naam: </label>
            <input value="<?php echo $team->getNaam(); ?>">
            </div>
        </fieldset>
        <fieldset>
            <legend>Wedstrijd</legend>
            <div>
            <label>Datum: </label>
            <input value="<?php echo $wedstrijd->getDatum(); ?>">
            </div>
        </fieldset>
    </form>
</head>
<body>
<h1>Leren werken met PHP</h1>
</body>
</html>