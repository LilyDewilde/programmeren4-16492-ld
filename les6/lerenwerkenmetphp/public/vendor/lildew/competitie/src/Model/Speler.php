<?php
namespace LilDew\Competitie\Model;

class Speler 
{
    private $familienaam;
    private $voorname;
    private $adres;
    private $rugnummer;
    
    public function setFamilienaam($familienaam) 
    {
        
        //je gebruikt een setter als je wilt controleren
        //of de waarde die je wilte toekennen aan een veld
        //een geschikte waarde is 
        //indien dat niet nodig is, maak je het veld public
        if (empty($familienaam)) {
            return false;
        } else {
            $this->familienaam = $familienaam; 
            return true;
        }
     }
    
    public function getFamilienaam() 
    {
        return $this->familienaam;
    }
    
    public function setVoornaam($voornaam) 
    {
        if (empty ($voornaam))
        {
            return false;
        } else {
            $this->voornaam = $voornaam;
            return true;
        }
    }
    
    public function getVoornaam() 
    {
        return $this->voornaam;
    }
    
    
    
    public function setAdres($adres) 
    {
        
        if (empty ($adres))
        {
            return false;
        } else {
            $this->adres = $adres;
            return true;
        }
    }
    
    public function getAdres() 
    {
        return $this->adres;
    }
    
    public function setRugnummer($Rugnummer) 
    {
        if (empty ($Rugnummer))
        {
            return false;
        } else {
            $this->Rugnummer = $Rugnummer;
            return true;
        }
    }
    
    public function getRugnummer() 
    {
        return $this->Rugnummer;
    }
}