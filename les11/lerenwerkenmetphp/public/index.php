<?php
    include __DIR__ . '/vendor/autoload.php';
    //strrpos: find last occurrence of substring
    $pos = strrpos($_SERVER['SCRIPT_NAME'], '/');
    $path = substr($_SERVER['SCRIPT_NAME'], 0, $pos + 1);

    echo 'pad: ' . $path;
    //str_replace: find arg a, replace by arg b, in arg c
    $uc = strtolower(str_replace($path, '', $_SERVER['REDIRECT_URL']));
    echo '<br/>' . 'uc: ' . $uc;
    //explode: string split
    $ucArray = explode('/' , $uc);
    echo '<br><span style="color: red;">';
    $view = __DIR__ . '/vendor/lildew/competitie/src/View/Admin/Index.php';
    switch ($ucArray[0])
    {
        case 'player' : {
            switch ($ucArray[1]) {
                case 'create' :
                    echo 'je gaat een speler aanmaken';
                    break;
                 case 'readingone' :
                     $player = new \LilDew\Competition\Model\Player();
                     $player->setLastName('Inghelbrecht');
                     $player->setFirstName('Jef');
                     $player->setAddress('Monaco');
                     $player->setShirtNumber('1');
                     $view = __DIR__ . '/vendor/lildew/competitie/src/View/Player/ReadingOne.php';
                     break;
                case 'updatingone' :
                    // in het echt lezen we de gegevens uit de database in
                    // connectie met mysql
                     $player = new \LilDew\Competition\Model\Player();
                     $player->setLastName('Inghelbrecht');
                     $player->setFirstName('Jef');
                     $player->setAddress('Monaco');
                     $player->setShirtNumber('1');
                     $view = __DIR__ . '/vendor/lildew/competitie/src/View/Player/UpdatingOne.php';
                    break;
                 case 'creatingone' :                     
                    $view = __DIR__ . '/vendor/lildew/competitie/src/View/Player/CreatingOne.php';
                    break;                
                 case 'editing' :                     
                    $view = __DIR__ . '/vendor/lildew/competitie/src/View/Player/Editing.php';
                    break;                
                case 'delete' :
                    echo 'je gaat een speler deleten';
                    break;
          }
        }
        case 'liga' : {
            switch ($ucArray[1]) {
                case 'create' :
                    echo 'je gaat een speler aanmaken';
                    break;
                 case 'read' :
                    echo 'je gaat een speler inlezen';
                    break;
                case 'update' :
                    echo 'je gaat een speler updaten';
                    break;
                 case 'delete' :
                    echo 'je gaat een speler deleten';
                    break;
          }
        }
    }
    echo '</span>';

    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Routing</title>
    <link rel="stylesheet" href="css/app.css" type="text/css" />
</head>
<body>
    <h1>Lokaal</h1>
    <form action="plopperdeplop.php" method="post">
        <input type="text" name="" id="">
        <input type="text" name="" id="">
        <button name="action" value="index">Inserten</button>
    </form>
    <p>$_SERVER['SCRIPT_NAME']: <?php echo $_SERVER['SCRIPT_NAME'];?></p>
    <p>$_SERVER['REQUEST_URI']: <?php echo $_SERVER['REQUEST_URI'];?></p>
    <p>$_SERVER['REDIRECT_URL']: <?php echo $_SERVER['REDIRECT_URL'];?></p>
    <pre>
        <?php print_r($_SERVER);
        echo 'ucarray: '; 
        print_r($ucArray);
    ?>
    </pre>
    <?php echo $view; include ($view);?>
</body>
</html>