<?php
// globale variabele bevat info over http request
print_r($_REQUEST);
// ervoor zorgen dat je altijd een default waarde in $useCase staan hebt
$useCase = 'Admin/Index';
// isset is een methode om na te gaan of
// bepaald element in de associatieve array bestaat, aka set is
if (isset($_REQUEST['uc'])) {
    $useCase = $_REQUEST['uc'];
}

echo 'use case in de querystring ' . $useCase;
// primitieve controller of dispatcher
switch ($useCase) {
    case 'Admin/Index':
        $path = 'vendor/lildew/competitie/src/View/Admin/Index.php';
        break;

    case 'Player/Editing':
        $path = 'vendor/lildew/competitie/src/View/Player/Editing.php';
        break;
    case 'Game/Editing':
        $path = 'vendor/lildew/competitie/src/View/Game/Editing.php';
        break;
    case 'Team/Editing':
         $path = 'vendor/lildew/competitie/src/View/Team/Editing.php';
        break;
    case 'Liga/Editing':
         $path = 'vendor/lildew/competitie/src/View/Liga/Editing.php';
        break;
}
echo 'pad: ' + $path;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <title>PHP</title>
</head>
<body>
    <?php include($path); ?>
    <pre>
        <?php print_r($_SERVER); ?>

    </pre>
</body>
</html>