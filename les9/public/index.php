<?php
    include __DIR__ . '/vendor/autoload.php';

    // instantie creëren van de getallen klasse
    $getallen = new \LilDew\LerenWerkenMetPHP\Getallen;
    $getallen->declareNumbers();
    $getallen->boolean();
    
    
    $tekst = new \LilDew\LerenWerkenMetPHP\Tekst;
    $tekst-> stringDeclareren();
    $tekst-> stringAanElkaarPlakken();
    
?>
