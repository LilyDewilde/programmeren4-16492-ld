<?php
namespace LilDew\LerenWerkenMetPHP;

class Getallen
{
    public function declareNumbers()
    {
        $integer = 5;
        $decimal = 56.3;
        $decimal2 = 56.30;
        $float = 0.774422;
        $float2 = 16777.216;
        $null = 0;
        $negative = -213;
        $integer2 = 1298317;
        $negativeInteger = -9912111;
        $negativeFloat = -12.52222;
        $decimal3 = 0.00;
        
        echo $integer * $integer . '<br>';
        echo $integer / $integer . '<br>';
        echo $integer - $integer . '<br>';
        echo $integer + $integer . '<br>';
        echo pow($integer, $integer . '<br>');
    }
    
    public function Boolean()
    {
        $b = false;
        var_dump($b . '<br>');
 
     
        $b = true;
        var_dump($b . '<br>');
        
        echo $b . '<br>';
        // Output: 1;
    }
}
