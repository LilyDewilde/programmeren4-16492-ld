<?php
namespace LilDew\LerenWerkenMetPHP;

class Tekst
{
    public function stringDeclareren()
    {
        $tekst = '"Ik ben aan het werken," zie ze.';
        print $tekst . '<br>';
    }
    
    public function stringAanElkaarPlakken()
    {
        $groet = 'Een goede morgen';
        $aan = 'Jan';
        $wens = 'toegewenst.';
        $zin = $groet . ' ' . $$aan . '  ' . $wens;
        echo $zin . '<br>';
    }
    
    public function stringInterpolatie()
    {
        $groet = 'Een goede morgen';
        $aan = 'Jan';
        $wens = 'toegewenst.';
        $zin = "$groet $aan $wens";
        echo $zin . '<br>';
    }
    
    public function tekstValideren()
    {
        $zipcode = trim($_POST['zipcode']);

        $zip_length = strlen($zipcode);
        if (strlen(trim($_POST['zipcode'])) != 4) {
        print 'Een postcode bestaat uit maximum 4 karakter.' . '<br>';}
    }
    
    public function stringsVergelijken()
    {
        $email = trim($_POST['email']);

        if($_POST['email'] == 'bob.dylan@telenet.be') {
        print 'Welkom, Bob Dylan.' . '<br>';}
    }
    
    public function strCaseCmp()
    {
        $name = trim($_POST['name']);
         
        if (strcasecmp($_POST['name'], 'Bob Dylan') == 0) {
        print "Welkom terug, Bob Dylan!" . '<br>';}
    }
    
    public function tekstOpmaken()
    {
        echo sprintf(currentDateTime());
        
        $price = 5;
        $tax = 12;
        echo sprintf('De maaltijd kost $%.2f', $price  +  ($price/100 * $tax)) . '<br>';
    }
 
}