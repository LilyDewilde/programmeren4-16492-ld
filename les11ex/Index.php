<?php
    // zorg ervoor dat het pad naar de subdirectory waar
    // de gereroute pagina naar toegaat
    // verwijderd wordt
    $pos = strrpos($_SERVER['SCRIPT_NAME'], '/');
    $path = substr($_SERVER['SCRIPT_NAME'], 0, $pos + 1);

    echo 'pad: ' . $path;
    $uc = strtolower(str_replace($path, '', $_SERVER['REDIRECT_URL']));
    echo '<br/>' . 'uc: ' . $uc;
    $ucArray = explode('/' , $uc);
    echo '<br><span style="color: red;">';
    switch ($ucArray[0])
    {
        case 'speler' : {
            switch ($ucArray[1]) {
                case 'create' :
                    echo 'je gaat een speler aanmaken';
                    break;
                 case 'read' :
                    echo 'je gaat een speler inlezen';
                    break;
                case 'update' :
                    echo 'je gaat een speler updaten';
                    break;
                 case 'delete' :
                    echo 'je gaat een speler deleten';
                    break;
          }
        }
        case 'liga' : {
            switch ($ucArray[1]) {
                case 'create' :
                    echo 'je gaat een liga aanmaken';
                    break;
                 case 'read' :
                    echo 'je gaat een liga inlezen';
                    break;
                case 'update' :
                    echo 'je gaat een liga updaten';
                    break;
                 case 'delete' :
                    echo 'je gaat een liga deleten';
                    break;
          }
        }
    }
    echo '</span>';

    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Routing</title>
</head>
<body>
    <h1>Lokaal</h1>
    <form action="plopperdeplop.php" method="post">
        <input type="text" name="" id="">
        <input type="text" name="" id="">
        <button name="action" value="index">Inserten</button>
    </form>
    <p>$_SERVER['SCRIPT_NAME']: <?php echo $_SERVER['SCRIPT_NAME'];?></p>
    <p>$_SERVER['REQUEST_URI']: <?php echo $_SERVER['REQUEST_URI'];?></p>
    <p>$_SERVER['REDIRECT_URL']: <?php echo $_SERVER['REDIRECT_URL'];?></p>
    <pre>
        <?php print_r($_SERVER);?>
    </pre>
    <pre>
        <?php print_r($ucArray);?>
    </pre>
</body>
</html>